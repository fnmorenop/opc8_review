package p233_ch_05_dates_strings_and_localization.p234_working_with_dates_and_times.p235_creating_dates_and_times;

import java.time.*;

public class P244_WorkingWithPeriods {

    public static void main(String[] args) {

        String pattern = "%n%s%n%n";

        System.out.printf(pattern, "Basic Period");

        LocalDate start = LocalDate.of(2015, Month.JANUARY, 1);
        LocalDate end = LocalDate.of(2015, Month.MARCH, 30);
        performAnimalEnrichement(start, end);

        Period period = Period.ofMonths(1);
        System.out.println();
        performAnimalEnrichementPerioded(start, end, period);

        System.out.printf(pattern, "Factory Methods Period");

        Period annually = Period.ofYears(1);
        System.out.printf("annually: %s%n", annually);
        Period quarterly = Period.ofMonths(3);
        System.out.printf("quarterly: %s%n", quarterly);
        Period everyThreeWeeks = Period.ofWeeks(3);
        System.out.printf("everyThreeWeeks: %s%n", everyThreeWeeks);
        Period everyOtherDay = Period.ofDays(2);
        System.out.printf("everyOtherDay: %s%n", everyOtherDay);
        Period everyYearAndWeek = Period.of(1, 0, 7);
        System.out.printf("everyYearAndWeek: %s%n", everyYearAndWeek);
        Period zero = Period.ZERO;
        System.out.printf("zero: %s%n", zero);
        Period bigger = Period.of(0, 20, 47);
        System.out.printf("bigger: %s%n", bigger);

        System.out.printf(pattern, "Period and time");

        LocalDate date = LocalDate.of(2020, Month.JANUARY, 20);
        LocalTime time = LocalTime.of(6, 15);
        LocalDateTime dateTime = LocalDateTime.of(date, time);
        System.out.printf("dateTime: %s%n", dateTime);
        Period periodOfMonth = Period.ofMonths(1);
        date = date.plus(periodOfMonth);
        System.out.printf("date: %s%n", date);
        dateTime = dateTime.plus(periodOfMonth);
        System.out.printf("dateTime: %s%n", dateTime);
        //time = time.plus(periodOfMonth); // UnsupportedTemporalTypeException
        //System.out.printf("time: %s%n", time);

    }

    private static void performAnimalEnrichementPerioded(LocalDate start, LocalDate end, Period period) {
        LocalDate upTo = start;
        while (upTo.isBefore(end)){
            System.out.printf("%nGive me new toy: %s%n", upTo);
            upTo = upTo.plus(period);
        }
    }

    private static void performAnimalEnrichement(LocalDate start, LocalDate end) {
        LocalDate upTo = start;
        while (upTo.isBefore(end)) {
            System.out.printf("%nGive me new toy: %s%n", upTo);
            upTo = upTo.plusMonths(1);
        }
    }
}
