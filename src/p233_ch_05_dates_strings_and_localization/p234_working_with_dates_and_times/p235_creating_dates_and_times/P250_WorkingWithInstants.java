package p233_ch_05_dates_strings_and_localization.p234_working_with_dates_and_times.p235_creating_dates_and_times;

import java.time.*;
import java.time.temporal.ChronoUnit;

public class P250_WorkingWithInstants {
    public static void main(String[] args) {

        Instant now = Instant.now();
        System.out.printf("now: %s%n", now);

        int j = 0;
        long base = 1_000_000_000L;
        for (int i = 1; i < base/8; i++)
            if(base % i == 0) j++;

        Instant later = Instant.now();
        System.out.printf("later: %s%n", later);


        Duration duration = Duration.between(now, later);
        System.out.printf("Duration in mill: %,d%n", duration.toMillis());

        LocalDate date = LocalDate.of(2015, 5, 25);
        LocalTime time = LocalTime.of(11, 55, 00);
        ZoneId zone = ZoneId.of("US/Eastern");
        ZonedDateTime zonedDateTime = ZonedDateTime.of(date, time, zone);
        Instant instant = zonedDateTime.toInstant();
        System.out.printf("%nZonedDateTime: %s%n", zonedDateTime);
        System.out.printf("Instant: %s%n", instant);

//        instant = Instant.ofEpochSecond(Instant.EPOCH.getEpochSecond());
//        instant = Instant.ofEpochSecond(Instant.now().getEpochSecond());
        instant = Instant.ofEpochSecond(zonedDateTime.toEpochSecond());
        System.out.printf("%nEpoch Seconds: %s%n", instant);

        Instant nextDay = instant.plus(1, ChronoUnit.DAYS);
        System.out.printf("%nnextDay: %s%n", nextDay);
        Instant nextHour = instant.plus(1, ChronoUnit.HOURS);
        System.out.printf("nextHour: %s%n", nextHour);
//        Instant nextWeek = instant.plus(1, ChronoUnit.WEEKS); // UnsupportedTemporalTypeException: Unsupported unit: Weeks
//        System.out.printf("nextWeek: %s%n", nextWeek);
//        Instant nextMonth = instant.plus(1, ChronoUnit.MONTHS);
//        System.out.printf("nextMonth: %s%n", nextMonth); // Unsupported unit: Months


    }
}
