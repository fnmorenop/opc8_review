package p233_ch_05_dates_strings_and_localization.p234_working_with_dates_and_times.p235_creating_dates_and_times;

import java.time.*;

public class P237_OtherSpecificExamples {
    public static void main(String[] args) {

        String pattern = "%n%s%n%n";

        System.out.printf(pattern, "LocalDate");

        LocalDate date1 = LocalDate.of(2015, Month.JANUARY, 20);
        System.out.printf("date1: %s%n", date1);
        LocalDate date2 = LocalDate.of(2015, 1, 20);
        System.out.printf("date2: %s%n", date2);

        Month month = Month.JANUARY;
        System.out.printf("month: %s%n", month);

        // boolean b1 = month == 1; // Doesn't compile
        boolean b2 = month == Month.APRIL;
        System.out.printf("b2: %b%n", b2);


        System.out.printf(pattern, "LocalTime");

        LocalTime time1 = LocalTime.of(6, 15);
        System.out.printf("time1: %s%n", time1);

        LocalTime time2 = LocalTime.of(6, 15, 30);
        System.out.printf("time2: %s%n", time2);

        LocalTime time3 = LocalTime.of(6, 15, 30, 200);
        System.out.printf("time3: %s%n", time3);

        System.out.printf(pattern, "LocalDateTime");

        LocalDateTime dateTime1 = LocalDateTime.of(
                2015, Month.JANUARY, 20,
                6, 15, 30
        );
        System.out.printf("dateTime1: %s%n", dateTime1);

        LocalDateTime dateTime2 = LocalDateTime.of(
                date1, time3
        );
        System.out.printf("dateTime2: %s%n", dateTime2);

        LocalDateTime dateTime3 = LocalDateTime.of(
                dateTime2.toLocalDate(), LocalTime.now()
        );
        System.out.printf("dateTime3: %s%n", dateTime3);

        System.out.printf(pattern, "ZonedDateTime");

        ZoneId zone = ZoneId.of("US/Eastern");
        ZonedDateTime zoned1 = ZonedDateTime.of(
                2015, 1, 20,
                6, 15, 30, 200,
                zone
        );
        System.out.printf("zoned1: %s%n", zoned1);

        ZonedDateTime zoned2 = ZonedDateTime.of(date1, time3, zone);
        System.out.printf("zoned2: %s%n", zoned2);

        ZonedDateTime zoned3 = ZonedDateTime.of(dateTime1, zone);
        System.out.printf("zoned3: %s%n", zoned3);

        System.out.printf(pattern, "ID Zones");

        System.out.printf("ZoneId.systemDefault(): %s%n",
                ZoneId.systemDefault());

        System.out.printf(pattern, "ID Zones Available");
        ZoneId.getAvailableZoneIds().stream().
                filter(s -> s.contains("America") && s.contains("B")).
                sorted().
                forEach(System.out::println);

    }
}
