package p233_ch_05_dates_strings_and_localization.p234_working_with_dates_and_times.p235_creating_dates_and_times;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;

public class P248_ChronoUnitForDifferences {

    public static void main(String[] args) {

        LocalTime one = LocalTime.of(5, 15);
        LocalTime two = LocalTime.of(6, 30);
        LocalDate date = LocalDate.of(2016, 1, 20);

        LocalDateTime ld1 = LocalDateTime.of(date, two);
        LocalDateTime ld2 = LocalDateTime.of(2017, 2, 21, 19, 31);


        System.out.printf("Hours: %s%n", ChronoUnit.HOURS.between(one, two));
        System.out.printf("Minutes: %s%n", ChronoUnit.MINUTES.between(one, two));
//        System.out.printf("Minutes: %s%n", ChronoUnit.MINUTES.between(one, date)); // DateTime exception
        System.out.printf("Minutes: %s%n", ChronoUnit.MINUTES.between(one, LocalDateTime.of(date, two)));
        System.out.printf("Minutes: %,d%n", ChronoUnit.MINUTES.between(ld1, ld2));


    }
}
