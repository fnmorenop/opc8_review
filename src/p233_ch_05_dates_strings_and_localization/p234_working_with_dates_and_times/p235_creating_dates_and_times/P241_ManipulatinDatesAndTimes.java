package p233_ch_05_dates_strings_and_localization.p234_working_with_dates_and_times.p235_creating_dates_and_times;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;

public class P241_ManipulatinDatesAndTimes {

    public static void main(String[] args) {

        String pattern = "%n%s%n%n";

        System.out.printf(pattern, "Time forward manipulation");

        LocalDate date = LocalDate.of(2014, Month.JANUARY, 20);
        System.out.printf("date: %s%n", date);
        date = date.plusDays(2);
        System.out.printf("plusDays date: %s%n", date);
        date = date.plusWeeks(1);
        System.out.printf("plusWeeks date: %s%n", date);
        date = date.plusMonths(1);
        System.out.printf("plusMonths date: %s%n", date);
        date = date.plusYears(5);
        System.out.printf("plusYears date: %s%n", date);

        System.out.printf(pattern, "Time backward manipulation");

        date = LocalDate.of(2020, Month.JANUARY, 20);
        LocalTime time = LocalTime.of(5, 15);
        LocalDateTime dateTime = LocalDateTime.of(date, time);
        System.out.printf("dateTime: %s%n", dateTime);
        dateTime = dateTime.minusDays(1);
        System.out.printf("minusDays dateTime: %s%n", dateTime);
        dateTime = dateTime.minusHours(10);
        System.out.printf("minusHours dateTime: %s%n", dateTime);
        dateTime = dateTime.minusSeconds(30);
        System.out.printf("minusSeconds dateTime: %s%n", dateTime);
        dateTime = dateTime.minusYears(5);
        System.out.printf("minusYears dateTime: %s%n", dateTime);

        System.out.printf(pattern, "Time chaining");

        date = LocalDate.of(2020, Month.JANUARY, 20);
        time = LocalTime.of(5, 15);
        dateTime = LocalDateTime.of(date, time).
        minusDays(1).minusHours(10).minusSeconds(30);
        System.out.printf("dateTime: %s%n", dateTime);

    }
}
