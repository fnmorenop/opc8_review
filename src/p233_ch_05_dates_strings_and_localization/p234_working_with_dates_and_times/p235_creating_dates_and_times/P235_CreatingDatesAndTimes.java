package p233_ch_05_dates_strings_and_localization.p234_working_with_dates_and_times.p235_creating_dates_and_times;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZonedDateTime;

public class P235_CreatingDatesAndTimes {
    public static void main(String[] args) {

        LocalDate localDate = LocalDate.of(1981, 8, 26);
        System.out.printf("localDate: %s%n", localDate);

        LocalDate localDate2 = LocalDate.now();
        System.out.printf("localDate2: %s%n", localDate2);

        LocalTime localTime = LocalTime.of(12, 35, 56, 12254789);
        System.out.printf("localTime: %s%n", localTime);

        LocalTime localTime2 = LocalTime.now();
        System.out.printf("localTime2: %s%n", localTime2);

        LocalDateTime localDateTime = LocalDateTime.of(localDate, localTime);
        System.out.printf("localDateTime: %s%n", localDateTime);

        LocalDateTime localDateTime2 = LocalDateTime.of(1931, 12, 24, 18, 25, 40);
        System.out.printf("localDateTime2: %s%n", localDateTime2);

        LocalDateTime localDateTime3 = LocalDateTime.now();
        System.out.printf("localDateTime3: %s%n", localDateTime3);

        ZonedDateTime zonedDateTime = ZonedDateTime.now();
        System.out.printf("zonedDateTime: %s%n", zonedDateTime);

        //ZonedDateTime zonedDateTime2 = ZonedDateTime.;
        //System.out.printf("zonedDateTime2: %s%n", zonedDateTime2);

    }
}
