package p233_ch_05_dates_strings_and_localization.p234_working_with_dates_and_times.p235_creating_dates_and_times;

import java.awt.*;
import java.time.*;
import java.time.temporal.ChronoUnit;

public class P247_WorkingWithDurations {

    public static void main(String[] args) {
        String pattern = "%n%s%n%n";

        System.out.printf(pattern, "Basic Duration");

        Duration daily = Duration.ofDays(7);
        System.out.printf("daily: %s%n", daily);
        Duration hourly = Duration.ofHours(1);
        System.out.printf("hourly: %s%n", hourly);
        Duration everyMinute = Duration.ofMinutes(100);
        System.out.printf("everyMinute: %s%n", everyMinute);
        Duration everyTenSeconds = Duration.ofSeconds(1000);
        System.out.printf("everyTenSeconds: %s%n", everyTenSeconds);
        Duration everyMilli = Duration.ofMillis(1);
        System.out.printf("everyMilli: %s%n", everyMilli);
        Duration everyNano = Duration.ofNanos(1);
        System.out.printf("everyNano: %s%n", everyNano);

        System.out.printf(pattern, "Basic Duration - Using ChronoUnit");

        daily = Duration.of(7, ChronoUnit.DAYS);
        System.out.printf("daily: %s%n", daily);
        hourly = Duration.of(1, ChronoUnit.HOURS);
        System.out.printf("hourly: %s%n", hourly);
        everyMinute = Duration.of(100, ChronoUnit.MINUTES);
        System.out.printf("everyMinute: %s%n", everyMinute);
        everyTenSeconds = Duration.of(1000, ChronoUnit.SECONDS);
        System.out.printf("everyTenSeconds: %s%n", everyTenSeconds);
        everyMilli = Duration.of(1, ChronoUnit.MILLIS);
        System.out.printf("everyMilli: %s%n", everyMilli);
        everyNano = Duration.of(1, ChronoUnit.NANOS);
        System.out.printf("everyNano: %s%n", everyNano);

        Duration millenium = Duration.of(1, ChronoUnit.HALF_DAYS);
        System.out.printf("Half day: %s%n", millenium);

        System.out.printf(pattern, "Using Duration the same way as Period");

        LocalDate date = LocalDate.of(2015, 1, 20);
        LocalTime time = LocalTime.of(6, 15);
        LocalDateTime dateTime = LocalDateTime.of(date, time);
        Duration duration = Duration.ofHours(6);
        System.out.printf("DateTime plus duration: %s%n", dateTime.plus(duration));
        System.out.printf("Time plus duration: %s%n", time.plus(duration));
//        System.out.printf("Date plus duration: %s%n", date.plus(duration)); // UnsupportedTemporalTypeException

        duration = Duration.ofHours(23);
        System.out.printf("%nDateTime plus duration: %s%n", dateTime.plus(duration));
        System.out.printf("Time plus duration: %s%n", time.plus(duration));
//        System.out.printf("Date plus duration: %s%n", date.plus(duration)); // UnsupportedTemporalTypeException

        Period period = Period.ofDays(1);
        duration = Duration.ofDays(1);
        System.out.printf("%nDate plus period: %s%n", date.plus(period));
//        System.out.printf("%nDate plus duration: %s%n", date.plus(duration)); // Unsupported unit: Seconds


    }
}
